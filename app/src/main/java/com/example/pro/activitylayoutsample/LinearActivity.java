package com.example.pro.activitylayoutsample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class LinearActivity extends AppCompatActivity {
	TextView formTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_linear);
		formTitle = findViewById(R.id.formTitle);
		formTitle.setText(getString(R.string.Regist_fir));
	}
}
