package com.example.pro.activitylayoutsample.IntentSamle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.pro.activitylayoutsample.R;

public class OriginActivity extends AppCompatActivity implements View.OnClickListener {
	EditText username, password;
	Button ShowDetails;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_origin);
		bind();
		findViewById(R.id.showDetails).setOnClickListener(this);

	}
	void bind(){
		username = findViewById(R.id.username);
		password = findViewById(R.id.password);
	}

	@Override
	public void onClick(View view) {
		Intent DeIntent = new Intent(this, DestinationActivity.class);
		DeIntent.putExtra("username", username.getText().toString());
		DeIntent.putExtra("password", password.getText().toString());
		startActivity(DeIntent);
	}
}
