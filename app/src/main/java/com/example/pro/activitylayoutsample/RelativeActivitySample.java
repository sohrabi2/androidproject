package com.example.pro.activitylayoutsample;

import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RelativeActivitySample extends AppCompatActivity {
	Button save;
	EditText name;
	String TAG = "onClick_monitor";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_relative_sample);
		getBind();

	}

	public void getBind() {
		save = findViewById(R.id.saveButton);
		name = findViewById(R.id.enterName);
		save.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String nameValue = name.getText().toString();
				Log.d(TAG, "Name: " + nameValue);
			}

		});
	}
}

