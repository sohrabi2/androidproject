package com.example.pro.activitylayoutsample.IntentSamle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.pro.activitylayoutsample.R;

public class DestinationActivity extends AppCompatActivity {
	TextView result;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_destination);
		((TextView) findViewById(R.id.result)).setText(getIntent().getStringExtra("username") + "" + getIntent().getStringExtra("password"));
	}
}
